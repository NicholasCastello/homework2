/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package djf.viewslideshow;



import djf.AppTemplate;
import static djf.settings.AppPropertyType.NEXT_SLIDE_ICON;
import static djf.settings.AppPropertyType.PAUSE_SLIDESHOW_ICON;
import static djf.settings.AppPropertyType.PLAY_SLIDESHOW_ICON;
import static djf.settings.AppPropertyType.PREV_SLIDE_ICON;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import sc.data.SlideshowCreatorData;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import sc.SlideshowCreatorApp;
import sc.data.Slide;
import sc.workspace.SlideshowCreatorWorkspace;


/**
 *
 * @author nicholascastello
 */
public class ViewSlideShow extends Application{
    
    SlideshowCreatorApp app;
    
    public SlideshowCreatorWorkspace workspace;// = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
    public TableView slidesTableView;// = workspace.slidesTableView;
    public Slide selectedSlide;// = (Slide)slidesTableView.getSelectionModel().getSelectedItem();
    public SlideshowCreatorData data;// = (SlideshowCreatorData)app.getDataComponent();
   
    HashMap<String, Image> images;
    public static int currentIndex = 0;

    // THESE ARE OUR ONLY CONTROLS, A BUTTON AND A DISPLAY LABEL
    Button prevButton = new Button();
    Button playButton = new Button();
    Button pauseButton = new Button();
    Button nextButton = new Button();
    ArrayList<String> students = new ArrayList();
    final Label studentNameLabel = new Label();
    final ImageView studentImageView = new ImageView();
    
    boolean isPlaying = false;
    
    public ViewSlideShow(AppTemplate app){
        this.app = (SlideshowCreatorApp)app;
        workspace = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();
        slidesTableView = workspace.slidesTableView;
        selectedSlide = (Slide)slidesTableView.getSelectionModel().getSelectedItem();
        data = (SlideshowCreatorData)app.getDataComponent();
   
    }


    public Image loadImage(String imagePath, int width, int height) throws MalformedURLException {
	File file = new File(imagePath);
	URL fileURL = file.toURI().toURL();
	Image image = new Image(fileURL.toExternalForm(), width, height, false, false);
	return image;
    }
    
    public void setIcon(Button button, String icon) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(icon);
        Image buttonImage = new Image(imagePath);
	ImageView graphic = new ImageView(buttonImage);
        graphic.setFitWidth(20);
        graphic.setFitHeight(18);
        
	// set the graphic
        button.setGraphic(graphic);
	
    }
    
    public void updateButtons(boolean isPlaying){
        prevButton.setDisable(isPlaying);
        playButton.setDisable(isPlaying);
        pauseButton.setDisable(!isPlaying);
        nextButton.setDisable(isPlaying);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
	// SET THE TITLE
	primaryStage.setTitle(data.getSlideShowTitle());
        
	images = new HashMap();
	for (int i = 0; i < data.getSlides().size(); i++) {
            Slide currentSlide = data.getSlides().get(i);
	    String studentName = currentSlide.getFileName();
            int width = currentSlide.getCurrentWidth();
            int height = currentSlide.getCurrentHeight();
	    try {
		Image studentImage = loadImage(currentSlide.getPath(), width, height);
		images.put(studentName, studentImage);
		students.add(studentName);
	    } catch (Exception e) {
		System.out.println("ERROR: " + studentName);
	    }
	}
        
	// CUSTOMIZE OUR FONTS
	List<String> fontFamilies = javafx.scene.text.Font.getFamilies();
        for(int i = 0; i < fontFamilies.size(); i++){
            System.out.println(fontFamilies.get(i));
        }
	studentNameLabel.setFont(new Font("Chalkboard", 48));
	
	// PUT THE BUTTONS IN A TOOLBAR
	FlowPane buttonToolbar = new FlowPane();
	buttonToolbar.setAlignment(Pos.CENTER);
	buttonToolbar.getChildren().add(prevButton);
	buttonToolbar.getChildren().add(playButton);
        buttonToolbar.getChildren().add(pauseButton);
	buttonToolbar.getChildren().add(nextButton);
        
        
        pauseButton.setDisable(true);

	// CUSTOMIZE OUR IMAGE VIEW
	String startingStudent = students.get(currentIndex);
	Image startingStudentImage = images.get(startingStudent);
	studentNameLabel.setText(startingStudent);
	studentImageView.setImage(startingStudentImage);

	// PUT THEM IN A CONTAINER
	VBox root = new VBox();
	root.setAlignment(Pos.CENTER);
        root.getChildren().add(studentImageView);
	root.getChildren().add(studentNameLabel);
	root.getChildren().add(buttonToolbar);
	

	// AND PUT THE CONTAINER IN THE WINDOW (i.e. the "stage")
	Scene scene = new Scene(root, 600, 400);
	primaryStage.setScene(scene);

	// PROVIDE A RESPONSE TO BUTTON CLICKS
	EventHandler buttonsHandler = new EventHandler<ActionEvent>() {
	    @Override
	    public void handle(ActionEvent event) {
		Task<Void> task = new Task<Void>() {
		    @Override
		    protected Void call() throws Exception {
//			int maxCount = 1; // PREV AND NEXT BUTTONS INC BY 1
			if (event.getSource() == playButton){
                            isPlaying = true;
                            updateButtons(isPlaying);
                        }
                        if (event.getSource() == pauseButton){
			    isPlaying = false;
                            updateButtons(isPlaying);
                        }
                        if (event.getSource() == prevButton)
                        {
                            currentIndex -= 1;
                            if (currentIndex < 0)
                                currentIndex = students.size() - 1;
                        }
                        else if (event.getSource() == nextButton)
                        {
                            currentIndex += 1;
                            if(currentIndex >= students.size() - 1)
                                currentIndex = 0;
                        }
                        else
                        {
                            //currentIndex = pickRandomIndex();
                        }

                        // THIS WILL BE DONE ASYNCHRONOUSLY VIA MULTITHREADING
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
//                                if(isPlaying){
//                                    currentIndex = (currentIndex + 1)%data.getSlides().size();
//                                }
                                String student = students.get(currentIndex);
                                studentNameLabel.setText(student);
                                studentImageView.setImage(images.get(student));
                            }
                        });

                        // SLEEP EACH FRAME
                        
                        
			return null;
		    }
		};
		// THIS GETS THE THREAD ROLLING
		Thread thread = new Thread(task);
		thread.start();
                
	    }
	};
        
        //set the icons
        setIcon(prevButton, PREV_SLIDE_ICON.toString());
        setIcon(playButton, PLAY_SLIDESHOW_ICON.toString());
        setIcon(pauseButton, PAUSE_SLIDESHOW_ICON.toString());
        setIcon(nextButton, NEXT_SLIDE_ICON.toString());

	// REGISTER THE LISTENER WITH ALL 3 BUTTONS
	prevButton.setOnAction(buttonsHandler);
	playButton.setOnAction(buttonsHandler);
        pauseButton.setOnAction(buttonsHandler);
	nextButton.setOnAction(buttonsHandler);

        
        Thread runner = new Thread(new Runnable(){

            @Override
            public void run() {
                while(true){
                    if(isPlaying){
                        currentIndex = (currentIndex + 1)%data.getSlides().size();
                    }
                    String student = students.get(currentIndex);
                    studentNameLabel.setText(student);
                    studentImageView.setImage(images.get(student));
                    studentImageView.setX(data.getSlides().get(currentIndex).getCurrentX());
                    studentImageView.setY(data.getSlides().get(currentIndex).getCurrentY());


                    try {
                        if(isPlaying){
                            Thread.sleep(1000);
                        }
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }
            }
        });
        
        runner.start();

        
	// OPEN THE WINDOW
	primaryStage.show();
    }
}

