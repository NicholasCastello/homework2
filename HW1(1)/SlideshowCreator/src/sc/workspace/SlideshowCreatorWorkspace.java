package sc.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.EventType;
import sc.SlideshowCreatorApp;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import properties_manager.PropertiesManager;
import sc.SlideshowCreatorProp;
import static sc.SlideshowCreatorProp.ADD_ALL_IMAGES_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.ADD_IMAGE_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.CAPTION_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_HEIGHT_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_HEIGHT_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_WIDTH_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_WIDTH_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_X_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.CURRENT_Y_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.FILE_NAME_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.FILE_NAME_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.MOVE_IMAGE_DOWN_ICON;
import static sc.SlideshowCreatorProp.MOVE_IMAGE_UP_ICON;
import static sc.SlideshowCreatorProp.ORIGINAL_HEIGHT_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.ORIGINAL_WIDTH_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.PATH_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.REMOVE_IMAGE_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.TITLE_TEXT_FIELD_PROMPT_TEXT;
import static sc.SlideshowCreatorProp.UPDATE_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.X_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.Y_COLUMN_TEXT;
import sc.data.Slide;
import sc.data.SlideshowCreatorData;
import static sc.style.SlideshowCreatorStyle.CLASS_EDIT_BUTTON;
import static sc.style.SlideshowCreatorStyle.CLASS_EDIT_SLIDER;
import static sc.style.SlideshowCreatorStyle.CLASS_EDIT_TEXT_FIELD;
import static sc.style.SlideshowCreatorStyle.CLASS_PROMPT_LABEL;
import static sc.style.SlideshowCreatorStyle.CLASS_SLIDES_TABLE;
import static sc.style.SlideshowCreatorStyle.CLASS_UPDATE_BUTTON;

/**
 * This class serves as the workspace component for the TA Manager
 * application. It provides all the user interface controls in 
 * the workspace area.
 * 
 * @author Richard McKenna
 */
public class SlideshowCreatorWorkspace extends AppWorkspaceComponent {
    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    SlideshowCreatorApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    SlideshowCreatorController controller;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    HBox editImagesToolbar;
    public TextField titleTextField;
    Button addAllImagesInDirectoryButton;
    Button addImageButton;
    Button removeImageButton;
    Button moveImageUpButton;
    Button moveImageDownButton;
    
    // FOR THE SLIDES TABLE
    ScrollPane slidesTableScrollPane;
    public TableView<Slide> slidesTableView;
    TableColumn<Slide, StringProperty> fileNameColumn;
    TableColumn<Slide, IntegerProperty> currentWidthColumn;
    TableColumn<Slide, IntegerProperty> currentHeightColumn;
    TableColumn<Slide, IntegerProperty> columnX;
    TableColumn<Slide, IntegerProperty> columnY;

    // THE EDIT PANE
    GridPane editPane;
    Label fileNamePromptLabel;
    TextField fileNameTextField;
    Label pathPromptLabel;
    TextField pathTextField;
    Label captionPromptLabel;
    TextField captionTextField;
    Label originalWidthPromptLabel;
    TextField originalWidthTextField;
    Label originalHeightPromptLabel;
    TextField originalHeightTextField;
    Label currentWidthPromptLabel;
    Slider currentWidthSlider;
    Label currentHeightPromptLabel;
    Slider currentHeightSlider;
    Label currentXPromptLabel;
    Slider currentXSlider;
    Label currentYPromptLabel;
    Slider currentYSlider;
    Button updateButton;
    
    /**
     * The constructor initializes the user interface for the
     * workspace area of the application.
     */
    public SlideshowCreatorWorkspace(SlideshowCreatorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // LAYOUT THE APP
        initLayout();
        
        // HOOK UP THE CONTROLLERS
        initControllers();
        
        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
    }
    
    private void initLayout() {
        // WE'LL USE THIS TO GET UI TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // FIRST MAKE ALL THE COMPONENTS
        editImagesToolbar = new HBox();
        titleTextField = new TextField();
        titleTextField.setPromptText(props.getProperty(TITLE_TEXT_FIELD_PROMPT_TEXT));
        addAllImagesInDirectoryButton = new Button(props.getProperty(ADD_ALL_IMAGES_BUTTON_TEXT));
        addImageButton = new Button(props.getProperty(ADD_IMAGE_BUTTON_TEXT));
        removeImageButton = new Button(props.getProperty(REMOVE_IMAGE_BUTTON_TEXT));
        moveImageUpButton = new Button();
        setIcon(moveImageUpButton, MOVE_IMAGE_UP_ICON.toString());
        moveImageDownButton = new Button();
        setIcon(moveImageDownButton, MOVE_IMAGE_DOWN_ICON.toString());
        slidesTableScrollPane = new ScrollPane();
        slidesTableView = new TableView();
        fileNameColumn = new TableColumn(props.getProperty(FILE_NAME_COLUMN_TEXT));
        currentWidthColumn = new TableColumn(props.getProperty(CURRENT_WIDTH_COLUMN_TEXT));
        currentHeightColumn = new TableColumn(props.getProperty(CURRENT_HEIGHT_COLUMN_TEXT));
        columnX = new TableColumn(props.getProperty(X_COLUMN_TEXT));
        columnY= new TableColumn(props.getProperty(Y_COLUMN_TEXT));
        editPane = new GridPane();
        fileNamePromptLabel = new Label(props.getProperty(FILE_NAME_PROMPT_TEXT));
        fileNameTextField = new TextField();
        pathPromptLabel = new Label(props.getProperty(PATH_PROMPT_TEXT));
        pathTextField = new TextField();
        captionPromptLabel = new Label(props.getProperty(CAPTION_PROMPT_TEXT));
        captionTextField = new TextField();
        originalWidthPromptLabel = new Label(props.getProperty(ORIGINAL_WIDTH_PROMPT_TEXT));
        originalWidthTextField = new TextField();
        originalHeightPromptLabel = new Label(props.getProperty(ORIGINAL_HEIGHT_PROMPT_TEXT));
        originalHeightTextField = new TextField();
        currentWidthPromptLabel = new Label(props.getProperty(CURRENT_WIDTH_PROMPT_TEXT));
        currentWidthSlider = new Slider();
        currentHeightPromptLabel = new Label(props.getProperty(CURRENT_HEIGHT_PROMPT_TEXT));
        currentHeightSlider = new Slider();
        currentXPromptLabel = new Label(props.getProperty(CURRENT_X_PROMPT_TEXT));
        currentXSlider = new Slider();
        currentYPromptLabel = new Label(props.getProperty(CURRENT_Y_PROMPT_TEXT)); 
        currentYSlider = new Slider(); 
        updateButton = new Button(props.getProperty(UPDATE_BUTTON_TEXT));
        
        // THE EDIT BUTTONS START OUT DISABLED
        addAllImagesInDirectoryButton.setDisable(true);
        addImageButton.setDisable(true);
        removeImageButton.setDisable(true);
        moveImageUpButton.setDisable(true);
        moveImageDownButton.setDisable(true);
        
        
        // ARRANGE THE TABLE
        fileNameColumn = new TableColumn(props.getProperty(FILE_NAME_COLUMN_TEXT));
        currentWidthColumn = new TableColumn(props.getProperty(CURRENT_WIDTH_COLUMN_TEXT));
        currentHeightColumn = new TableColumn(props.getProperty(CURRENT_HEIGHT_COLUMN_TEXT));
        columnX = new TableColumn(props.getProperty(X_COLUMN_TEXT));
        columnY= new TableColumn(props.getProperty(Y_COLUMN_TEXT));
        slidesTableView.getColumns().add(fileNameColumn);
        slidesTableView.getColumns().add(currentWidthColumn);
        slidesTableView.getColumns().add(currentHeightColumn);
        slidesTableView.getColumns().add(columnX);
        slidesTableView.getColumns().add(columnY);
        fileNameColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(2));
        currentWidthColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(4));
        currentHeightColumn.prefWidthProperty().bind(slidesTableView.widthProperty().divide(4));
        columnX.prefWidthProperty().bind(slidesTableView.widthProperty().divide(8));
        columnY.prefWidthProperty().bind(slidesTableView.widthProperty().divide(8));
        fileNameColumn.setCellValueFactory(
                new PropertyValueFactory<Slide, StringProperty>("fileName")
        );
        currentWidthColumn.setCellValueFactory(
                new PropertyValueFactory<Slide, IntegerProperty>("currentWidth")
        );
        currentHeightColumn.setCellValueFactory(
                new PropertyValueFactory<Slide, IntegerProperty>("CurrentHeight")
        );
        columnX.setCellValueFactory(
                new PropertyValueFactory<Slide, IntegerProperty>("currentX")
        );
        columnY.setCellValueFactory(
                new PropertyValueFactory<Slide, IntegerProperty>("currentY")
        );
        // HOOK UP THE TABLE TO THE DATA
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        ObservableList<Slide> model = data.getSlides();
        slidesTableView.setItems(model);
        
        // SETUP THE SLIDERS
        currentWidthSlider.setMin(0);
        currentWidthSlider.setMax(1000);
        currentWidthSlider.setBlockIncrement(200);
        currentWidthSlider.setShowTickLabels(true);
        currentWidthSlider.setMajorTickUnit(200);
        currentWidthSlider.setSnapToTicks(true);
        currentWidthSlider.setMinorTickCount(6);
        currentWidthSlider.setShowTickMarks(true);
        currentHeightSlider.setMin(0);
        currentHeightSlider.setMax(1000);
        currentHeightSlider.setBlockIncrement(200);
        currentHeightSlider.setShowTickLabels(true);
        currentHeightSlider.setMajorTickUnit(200);
        currentHeightSlider.setSnapToTicks(true);
        currentHeightSlider.setMinorTickCount(6);
        currentHeightSlider.setShowTickMarks(true);
        currentXSlider.setMin(0);
        currentXSlider.setMax(1000);
        currentXSlider.setBlockIncrement(200);
        currentXSlider.setShowTickLabels(true);
        currentXSlider.setMajorTickUnit(200);
        currentXSlider.setSnapToTicks(true);
        currentXSlider.setMinorTickCount(6);
        currentXSlider.setShowTickMarks(true);
        currentYSlider.setMin(0);
        currentYSlider.setMax(1000);
        currentYSlider.setBlockIncrement(200);
        currentYSlider.setShowTickLabels(true);
        currentYSlider.setMajorTickUnit(200);
        currentYSlider.setSnapToTicks(true);
        currentYSlider.setMinorTickCount(6);
        currentYSlider.setShowTickMarks(true);
        
        // THEN ORGANIZE THEM
        editImagesToolbar.getChildren().add(titleTextField);
        editImagesToolbar.getChildren().add(addAllImagesInDirectoryButton);
        editImagesToolbar.getChildren().add(addImageButton);
        editImagesToolbar.getChildren().add(removeImageButton);
        editImagesToolbar.getChildren().add(moveImageUpButton);
        editImagesToolbar.getChildren().add(moveImageDownButton);
        slidesTableScrollPane.setContent(slidesTableView);
        editPane.add(fileNamePromptLabel, 0, 0);
        editPane.add(fileNameTextField, 1, 0);
        editPane.add(pathPromptLabel, 0, 1);
        editPane.add(pathTextField, 1, 1);
        editPane.add(captionPromptLabel, 0, 2);
        editPane.add(captionTextField, 1, 2);
        editPane.add(originalWidthPromptLabel, 0, 3);
        editPane.add(originalWidthTextField, 1, 3);
        editPane.add(originalHeightPromptLabel, 0, 4);
        editPane.add(originalHeightTextField, 1, 4);
        editPane.add(currentWidthPromptLabel, 0, 5);
        editPane.add(currentWidthSlider, 1, 5);
        editPane.add(currentHeightPromptLabel, 0, 6);
        editPane.add(currentHeightSlider, 1, 6);
        editPane.add(currentXPromptLabel, 0, 7);
        editPane.add(currentXSlider, 1, 7);
        editPane.add(currentYPromptLabel, 0, 8);
        editPane.add(currentYSlider, 1, 8);
        editPane.add(updateButton, 0, 9);
        
        // DISABLE THE DISPLAY TEXT FIELDS
        fileNameTextField.setDisable(true);
        pathTextField.setDisable(true);
        captionTextField.setDisable(true);
        originalWidthTextField.setDisable(true);
        originalHeightTextField.setDisable(true);
        currentWidthSlider.setDisable(true);
        currentHeightSlider.setDisable(true);
        currentXSlider.setDisable(true);
        currentYSlider.setDisable(true);
        updateButton.setDisable(true);
        
        // AND THEN PUT EVERYTHING INSIDE THE WORKSPACE
        app.getGUI().getTopToolbarPane().getChildren().add(editImagesToolbar);
        BorderPane workspaceBorderPane = new BorderPane();
        workspaceBorderPane.setCenter(slidesTableScrollPane);
        slidesTableScrollPane.setFitToWidth(true);
        slidesTableScrollPane.setFitToHeight(true);
        workspaceBorderPane.setRight(editPane);
        
        // AND SET THIS AS THE WORKSPACE PANE
        workspace = workspaceBorderPane;
    }
    
    private void initControllers() {
        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new SlideshowCreatorController(app);
        /*
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        
        titleTextField.focusedProperty().addListener({data.setSlideShowTitle(titleTextField.getText()})
                
                });*/
        
        titleTextField.textProperty().addListener(e->{
            controller.handleTitleTextTyped();
        });

        addAllImagesInDirectoryButton.setOnAction(e->{
            controller.handleAddAllImagesInDirectory();
        });
        addImageButton.setOnAction(e->{
            controller.handleAddImage();
        });
        removeImageButton.setOnAction(e->{
            controller.handleRemoveImage();
        });
        
        moveImageUpButton.setOnAction(e->{
            controller.handleMoveImageUp();
        });
        
        moveImageDownButton.setOnAction(e->{
            controller.handleMoveImageDown();
        });
        
        slidesTableView.getSelectionModel().selectedItemProperty().addListener(e->{
            controller.handleSelectSlide();
        });
        captionTextField.textProperty().addListener(e->{
            controller.handleCaptionTextTyped();
        });
        currentWidthSlider.valueProperty().addListener(e->{
            controller.handleSliderMoved();
        });
        currentHeightSlider.valueProperty().addListener(e->{
            controller.handleSliderMoved();
        });
        currentXSlider.valueProperty().addListener(e->{
            controller.handleSliderMoved();
        });
        currentYSlider.valueProperty().addListener(e->{
            controller.handleSliderMoved();
        });
        updateButton.setOnAction(e->{
            controller.handleUpdateSlide();
        });
    }
    
    
    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    
    private void initStyle() {
        editImagesToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        addAllImagesInDirectoryButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        addImageButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        removeImageButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        moveImageUpButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        moveImageDownButton.getStyleClass().add(CLASS_EDIT_BUTTON);

        // THE SLIDES TABLE
        slidesTableView.getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : slidesTableView.getColumns())
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        
        editPane.getStyleClass().add(CLASS_BORDERED_PANE);
        fileNamePromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        fileNameTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        pathPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        pathTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        captionPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        captionTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        originalWidthPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        originalWidthTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        originalHeightPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        originalHeightTextField.getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        currentWidthPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        currentWidthSlider.getStyleClass().add(CLASS_EDIT_SLIDER);
        currentHeightPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        currentXPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        currentXSlider.getStyleClass().add(CLASS_EDIT_SLIDER);
        currentYPromptLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        currentYSlider.getStyleClass().add(CLASS_EDIT_SLIDER);
        currentHeightSlider.getStyleClass().add(CLASS_EDIT_SLIDER);
        updateButton.getStyleClass().add(CLASS_UPDATE_BUTTON);
    }

    @Override
    public void resetWorkspace() {
        addAllImagesInDirectoryButton.setDisable(false);
        addImageButton.setDisable(false);
        SlideshowCreatorData data = (SlideshowCreatorData)app.getDataComponent();
        data.resetData();
        enableSlideEditingControls(false);
    }
    
    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        updateButton.setDisable(true);
    }

    public void enableSlideEditingControls(boolean enable) {
        // ENABLE THINGS THAT CAN BE USED
        captionTextField.setDisable(!enable);
        currentWidthSlider.setDisable(!enable);
        currentHeightSlider.setDisable(!enable);
        currentXSlider.setDisable(!enable);
        currentYSlider.setDisable(!enable);
        updateButton.setDisable(enable);
        removeImageButton.setDisable(!enable);
        moveImageUpButton.setDisable(!enable);
        moveImageDownButton.setDisable(!enable);

        // SHOULD WE CLEAR THE DATA TOO?
        if (!enable) {
            fileNameTextField.setText("");
            pathTextField.setText("");
            captionTextField.setText("");
            originalWidthTextField.setText("");
            originalHeightTextField.setText("");
            currentWidthSlider.setValue(0);
            currentHeightSlider.setValue(0);
            currentXSlider.setValue(0);
            currentYSlider.setValue(0);
        }
    }
    
    public void setIcon(Button button, String icon) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(icon);
        Image buttonImage = new Image(imagePath);
	ImageView graphic = new ImageView(buttonImage);
        graphic.setFitWidth(20);
        graphic.setFitHeight(18);
        
	// set the graphic
        button.setGraphic(graphic);
	
    }
}
